<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DashboardMessages extends Component
{
    public function render()
    {
        return view('livewire.dashboard-messages');
    }
}
