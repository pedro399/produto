<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DashboardCard extends Component
{
    public function render()
    {
        return view('livewire.dashboard-card');
    }
}
