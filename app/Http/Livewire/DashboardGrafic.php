<?php

namespace App\Http\Livewire;

use Livewire\Component;

class DashboardGrafic extends Component
{
    public function render()
    {
        return view('livewire.dashboard-grafic');
    }
}
