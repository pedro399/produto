<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ImovelCard extends Component
{
    public function render()
    {
        return view('livewire.imovel-card');
    }
}
