<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AgButton extends Component
{

    public $content;
    public $fontSize;
    public $bgColor;
    public $bgHover;
    public $icon = false;
    public $disabled;
    public $type;

    public function mount($fontSize, $content, $bgColor, $bgHover, $icon, $disabled, $type)
    {
        $this->content = $content;
        $this->fontSize = $fontSize;
        $this->bgColor = $bgColor; /* primary warning-2 error-2 success-2 */
        $this->bgHover = $bgHover; /* primary warning-3 error-3 success-3 */
        $this->icon = $icon;
        $this->disabled = $disabled;
        $this->type = $type;

    }
    public function render()
    {
        return view('livewire.ag-button');
    }
}
