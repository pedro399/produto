<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/all', function () {
    return view('all');
});


Route::get('/anuncio', function () {
    return view('anuncio');
});

Route::get('/uikit', function () {
    return view('uikit');
});

Route::get('/galeria', function () {
    return view('galeria');
});

Route::get('/agendamento', function () {
    return view('agendamento');
});

Route::get('/corretores/buscar', function () {
    return view('corretores/buscar');
});

Route::get('/corretores/visitas', function () {
    return view('corretores/visitas');
});
