<input
    name="{{ $name }}"
    type="{{ $type }}"
    id="{{ $id }}"
    @if($value)value="{{ $value }}"@endif
    {{ $attributes }}
    class="max-w-xs h-12 border-neutral-4 border-1.5 rounded-md px-4 py-3
            focus:outline-none"
/>
