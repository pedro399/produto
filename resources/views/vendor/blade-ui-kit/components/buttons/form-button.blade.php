<form method="POST" action="{{ $action }}">
    @csrf
    @method($method)

    {{--    14         16        20  --}}
    {{-- text-Xxxs  text-Xxs  text-Sm--}}
    @props(['size' => 'Sm'])

        <button
            {{ $attributes->merge([
                    'class' => "px-4 py-3 bg-primary text-white text-{$size} font-SemiBold
                    leading-3 tracking-wider rounded-lg shadow-1
                    focus:outline-none"]) }}>
            {{ $slot }}
        </button>

</form>
