{{-- <x-html title="Galeria" class="font-poppins">

    <x-slot name="head">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
            rel="stylesheet">

        <script defer src="https://unpkg.com/alpinejs@3.2.1/dist/cdn.min.js"></script>

    </x-slot>

    <a href="img/planta_baixa/1.png" data-lightbox="1" data-title="My caption">Image #1</a>


</x-html> --}}
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <script src="https://unpkg.com/alpinejs@3.2.1/dist/cdn.min.js"></script>
</head>

<body class=" font-poppins">
    <div class="container p-10">

        <div class="grid grid-cols-1 md:grid-cols-3 gap-6 p-6 rounded-md bg-white shadow-5 relative max-w-3xl m-auto">
            <div><img src="img/1.png" class="absolute left-0 inset-y-0 object-cover rounded-l-md" style="width: 14.9rem; height: 15.6rem;"></div>

            <div class="flex flex-col gap-6">
                <div>
                    <p class=" font-semibold text-neutral-1 leading-4 mb-1">Menino Deus, Porto Alegre</p>
                    <p class=" text-neutral-3 text-Xxxs leading-4 mb-1">Rua Visconde do Herval, 204</p>
                    <p class=" text-neutral-3 text-Xxxs leading-4">Apartamento</p>
                </div>

                <div>
                    <p class=" font-semibold text-primary text-xl leading-3 mb-1">R$ 1.896.000,00</p>
                    <p class=" text-neutral-3 text-Xxxs leading-4">Comissão R$ 22.000,00 - 4%</p>
                    {{-- <p class=" text-neutral-3 leading-4 text-Xxxs mb-4">Captação disponibilizada por corretor/imobiliária
                        parceira</p> --}}
                </div>

                <div class="flex justify-between">
                    <label class="flex text-neutral-4">
                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                    </label>
                    <label class="flex text-neutral-4">
                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                    </label>
                    <label class="flex text-neutral-4">
                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                    </label>
                </div>
            </div>

            <div class="grid grid-cols-1">
                <a @click="open_agend=true" class="flex items-center justify-center bg-primary py-3 px-4
                    rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8
                    tracking-wider shadow-1
                    mb-3 cursor-pointer
                    hover:bg-primary-3
                    focus:outline-none
                    disabled:bg-primary-6 disabled:pointer-events-none">
                    Agendar visita
                </a>

                <a type="button" class="flex items-center justify-center py-3 px-4
                    rounded-pill leading-3 font-semibold text-Xxxs text-primary
                    tracking-wider ring-inset cursor-pointer
                    hover:text-primary-3 hover:border-primary-3
                    focus:outline-none focus:bg-neutral-7
                    disabled:bg-primary-6 disabled:text-primary-6
                    disabled:pointer-events-none">
                    Informações pré-visita >
                </a>

                <div class="row-span-4"></div>

                <div>
                    <a class="flex items-center justify-center p-3 shadow-5 rounded-full cursor-pointer float-right">
                        <img src="img/3pontosvertcal.svg" alt="error" class="">
                    </a>
                </div>
            </div>
        </div>

    </div>
</body>

</html>
