<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Componentes Gerais</title>

    <!-- Fonts -->
    {{-- <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}

    @livewireStyles
</head>

<style>
    body {
        font-family: 'Poppins'
    }

</style>

<body>
    {{-- Botoes --}}
    <section class=" m-8">
        <div class=" text-neutral-2 text-2xl leading-4 font-bold my-3">Botões DS</div>
        <div class="grid grid-cols-1 gap-10 md:grid-cols-2 lg:grid-cols-3 ">
            {{-- Primary --}}

            {{-- Text --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Primary (hover - focus - disabled) / Text / Big --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Sm text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        focus:outline-none
                        disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Sm text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                    </button>
                </div>


                {{-- Primary (hover - focus - disabled) / Text / Medium --}}

                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        focus:outline-none
                        disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                    </button>
                </div>

                {{-- Primary (hover - focus - disabled) / Text / Small --}}

                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        focus:outline-none
                        disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                        rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8
                        tracking-wider shadow-1
                        hover:bg-primary-3
                        disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                    </button>
                </div>

            </div>
            {{-- Text - Icon --}}
            <div class="grid grid-cols-2 gap-4">

                {{-- Primary (hover - focus - disabled) / Icon / Big --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                            rounded-pill leading-3 font-semibold text-Sm text-neutral-8
                            tracking-wider shadow-1
                            hover:bg-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                            rounded-pill leading-3 font-semibold text-Sm text-neutral-8
                            tracking-wider shadow-1
                            hover:bg-primary-3
                            disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>

                {{-- Primary (hover - focus - disabled) / Icon / Medium --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                                            rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                                            tracking-wider shadow-1
                                            hover:bg-primary-3
                                            focus:outline-none
                                            disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                                            rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                                            tracking-wider shadow-1
                                            hover:bg-primary-3
                                            disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>

                {{-- Primary (hover - focus - disabled) / Icon / Small --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                                            rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8
                                            tracking-wider shadow-1
                                            hover:bg-primary-3
                                            focus:outline-none
                                            disabled:bg-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center bg-primary py-2 px-4
                                            rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8
                                            tracking-wider shadow-1
                                            hover:bg-primary-3
                                            disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
            </div>
            {{-- Icon --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Primary (hover - focus - disabled) / Icon / Big --}}
                <div class=" w-7">
                    <button type="button" class="flex items-center justify-center bg-primary p-4
                            rounded-full leading-3 font-semibold text-Sm text-neutral-8
                            tracking-wider shadow-1
                            hover:bg-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
                <div class=" w-7">
                    <button type="button" class="flex items-center justify-center bg-primary p-4
                            rounded-full leading-3 font-semibold text-Sm text-neutral-8
                            tracking-wider shadow-1
                            hover:bg-primary-3
                            disabled:bg-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="white" />
                        </svg>
                    </button>
                </div>
            </div>

            {{-- Secundary --}}

            {{-- Text --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Secundary (hover - focus - disabled) / Text / Big --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Sm text-primary
                                    tracking-wider border-2 border-primary ring-inset
                                    hover:text-primary-3 hover:border-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Sm text-primary
                                    tracking-wider border-2 border-primary ring-inset
                                    hover:text-primary-3 hover:border-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Text / Medium --}}
                <div class=" w-28">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                    tracking-wider border-2 border-primary ring-inset
                                                    hover:text-primary-3 hover:border-primary-3
                                                    focus:outline-none focus:bg-neutral-7
                                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                    tracking-wider border-2 border-primary ring-inset
                                                    hover:text-primary-3 hover:border-primary-3
                                                    focus:outline-none focus:bg-neutral-7
                                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Text / Small --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                    tracking-wider border-2 border-primary ring-inset
                                    hover:text-primary-3 hover:border-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                    tracking-wider border-2 border-primary ring-inset
                                    hover:text-primary-3 hover:border-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>
            </div>

            {{-- Text - Icon --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Secundary (hover - focus - disabled) / Icon / Big --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Sm text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Sm text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Icon / Medium --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Xxs text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Xxs text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Icon / Small --}}
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Xxxs text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                               rounded-pill leading-3 font-semibold text-Xxxs text-primary
                               tracking-wider border-2 border-primary ring-inset
                               hover:text-primary-3 hover:border-primary-3
                               focus:outline-none focus:bg-neutral-7
                               disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>
            </div>

            {{-- Icon --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Secundary (hover - focus - disabled) / Icon / Big --}}
                <div class=" w-7">
                    <button type="button" class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Sm text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-7">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Sm text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Icon / Medium --}}
                <div class=" w-7">
                    <button type="button" class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Xxs text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-7">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Xxs text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Secundary (hover - focus - disabled) / Icon / Small --}}
                <div class=" w-7">
                    <button type="button" class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-7">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                           rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                           tracking-wider border-2 border-primary ring-inset
                                           hover:text-primary-3 hover:border-primary-3
                                           focus:outline-none focus:bg-neutral-7
                                           disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>
            </div>

            {{-- Tertiary --}}

            {{-- Text --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Tertiary (hover - focus - disabled) / Text / Big --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Sm text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Sm text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text / Mediun --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text / Small --}}
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                    </button>
                </div>
                <div class=" w-28">
                    <button type="button" class="flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                    tracking-wider
                                    hover:text-primary-3
                                    focus:outline-none focus:bg-neutral-7
                                    disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                        disabled>
                        Button
                    </button>
                </div>
            </div>

            {{-- Text - Icon --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Big --}}
                <div class="w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Sm text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Sm text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Mediun --}}
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Small --}}
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center py-2 px-4
                                                rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        Button
                        <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>
            </div>

            {{-- Icon --}}
            <div class="grid grid-cols-2 gap-4">
                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Big --}}
                <div class="w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Sm text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Sm text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Mediun --}}
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Xxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>

                {{-- Tertiary (hover - focus - disabled) / Text - Icon / Small --}}
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5 fill-current text-primary-6"
                            viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#8629cf" />
                        </svg>
                    </button>
                </div>
                <div class=" w-32">
                    <button type="button"
                        class="flex items-center justify-center p-4
                                                rounded-pill leading-3 font-semibold text-Xxxs text-primary
                                                tracking-wider
                                                hover:text-primary-3
                                                focus:outline-none focus:bg-neutral-7
                                                disabled:border-primary-6 disabled:text-primary-6 disabled:pointer-events-none" disabled>
                        <svg xmlns="http://www.w3.org/2000/svg" class="w-5 h-5" viewBox="0 0 24 16" fill="none">
                            <path
                                d="M19.35 6.04C18.67 2.59 15.64 0 12 0C9.11 0 6.6 1.64 5.35 4.04C2.34 4.36 0 6.91 0 10C0 13.31 2.69 16 6 16H19C21.76 16 24 13.76 24 11C24 8.36 21.95 6.22 19.35 6.04ZM14 9V13H10V9H7L11.65 4.35C11.85 4.15 12.16 4.15 12.36 4.35L17 9H14Z"
                                fill="#d7a5ff" />
                        </svg>
                    </button>
                </div>
            </div>

        </div>
    </section>

    {{-- INPUTS --}}
    <section class="m-8">
        <div class=" text-neutral-2 text-2xl leading-4 font-bold my-3">Inputs DS</div>

        <div class=" text-neutral-4 text-Sm leading-4 font-bold my-3">Inputs DS</div>

        <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4">

            {{-- INPUTS --}}

            {{-- primary --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                group-hover:text-primary
                                group-focus:text-primary
                                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="mt-2 w-56 h-12 border border-gray-400 rounded-md
                                group-hover:border-primary
                                group-focus:border-primary
                                focus:outline-none p-4
                                disabled:border-neutral-5">
            </div>

            {{-- error --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                group-hover:text-primary
                                group-focus:text-primary">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="mt-2 w-56 h-12 border border-error-2 rounded-md
                                group-hover:border-error-2
                                group-focus:border-error-2
                                focus:outline-none p-4">
            </div>

            {{-- primary + help text --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                        group-hover:text-primary
                                        group-focus:text-primary">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="mt-2 w-56 h-12 border border-gray-400 rounded-md
                                        group-hover:border-primary
                                        group-focus:border-primary
                                        focus:outline-none p-4">
                <div class=" text-Caption text-neutral-4 font-medium">Aqui vai um texto de ajuda!</div>
            </div>

            {{-- error + help text --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                        group-hover:text-primary
                                        group-focus:text-primary">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="mt-2 w-56 h-12 border border-error-2 rounded-md
                                        group-hover:border-error-2
                                        group-focus:border-error-2
                                        focus:outline-none p-4">
                <div class=" text-Caption text-error-2 font-medium">Aqui vai um texto de ajuda!</div>
            </div>

            {{-- Left icon --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                group-hover:text-primary
                                group-focus:text-primary
                                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="z-0 mt-2 w-56 h-12 border border-gray-400 rounded-md
                                relative pl-12
                                group-hover:border-primary
                                group-focus:border-primary
                                focus:outline-none p-4
                                disabled:border-neutral-5">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-neutral-4 absolute bottom-0 left-0 z-50"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" fill="none"
                        d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                </svg>
            </div>

            {{-- Right Icon --}}
            <div class="group w-56 h-24">
                <div class="leading-6 text-neutral-4
                                group-hover:text-primary
                                group-focus:text-primary
                                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel</div>
                <input type="text" placeholder="Escrever ID" class="z-0 mt-2 w-56 h-12 border border-gray-400 rounded-md
                                relative
                                group-hover:border-primary
                                group-focus:border-primary
                                focus:outline-none p-4
                                disabled:border-neutral-5">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-neutral-4 absolute bottom-0 right-0 z-50"
                    fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" fill="none"
                        d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21" />
                </svg>
            </div>

            <div class="relative p-4 bg-error-2">
                <p>teste</p>
                <div class="absolute p-4 bg-warning-2">
                    <p>teste</p>
                </div>
            </div>

        </div>

        <div class=" text-neutral-4 text-Sm leading-4 font-bold my-3">Textarea DS</div>

        <div class="grid grip-cols-1 md:grid-cols-2 lg:grid-cols-3">
            <div class="group">
                <div class="leading-6 text-neutral-4 mb-2
                group-hover:text-primary
                group-focus:text-primary
                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel
                </div>
                <textarea class="border border-neutral-4 rounded-md py-1 px-4 text-neutral-4
                group-hover:border-primary group-hover:text-primary
                group-focus:border-primary
                focus:outline-none p-4
                disabled:border-neutral-5" cols="43" rows="5">Insira aqui seu texto de descrição</textarea>
            </div>

            <div class="group">
                <div class="leading-6 text-neutral-4 mb-2
                group-hover:text-primary
                group-focus:text-primary
                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel
                </div>
                <textarea class="border border-error-2 rounded-md py-1 px-4 text-neutral-4
                group-hover:border-error-2 group-hover:text-primary
                group-focus:border-error-2
                focus:outline-none p-4
                disabled:border-neutral-5" cols="43" rows="5">Insira aqui seu texto de descrição</textarea>
            </div>
        </div>


        <div class=" text-neutral-4 text-Sm leading-4 font-bold my-3">Dropdowns DS</div>

        <div class="grid grip-cols-1 md:grid-cols-2 lg:grid-cols-4">
            <div class="group w-72 h-24   ">
                <div class="leading-6 text-neutral-4 mb-2
                                group-hover:text-primary
                                group-focus:text-primary
                                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel
                </div>
                <button class="w-72 h-12 border border-gray-400 rounded-top relative leading-6 text-neutral-4
                                group-hover:border-primary group-hover:text-primary
                                group-focus:border-primary
                                focus:outline-none p-4
                                disabled:border-neutral-5">
                    Selecione um elemento
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-neutral-4 absolute right-4 bottom-2
                    group-hover:border-primary
                    group-focus:border-primary" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                    </svg>
                </button>

                <ul class=" bg-white border border-neutral-7 rounded-botton pointer-events-auto">
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75">Batman</li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75">Arrow</li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75">Pink</li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75">Thor</li>
                </ul>

            </div>

            <div class="group w-72 h-24">
                <div class="leading-6 text-neutral-4
                                group-hover:text-primary
                                group-focus:text-primary
                                disabled:text-neutral-5 disabled:bg-white">
                    ID do imóvel
                </div>
                <button class="mt-2 w-72 h-12 border border-gray-400 rounded-top relative leading-6 text-neutral-4
                                group-hover:border-primary group-hover:text-primary
                                group-focus:border-primary
                                focus:outline-none p-4
                                disabled:border-neutral-5">
                    Selecione um elemento
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-neutral-4 absolute right-4 bottom-2
                    group-hover:border-primary
                    group-focus:border-primary" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                    </svg>
                </button>

                <ul class=" bg-white border border-neutral-7 rounded-botton pointer-events-auto">
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">
                        <img src="https://via.placeholder.com/400x150" class=" w-7 h-7 rounded-circular mr-2"
                            alt="user">
                        Batman
                    </li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">
                        <img src="https://via.placeholder.com/400x150" class=" w-7 h-7 rounded-circular mr-2"
                            alt="user">
                        Arrow
                    </li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">
                        <img src="https://via.placeholder.com/400x150" class=" w-7 h-7 rounded-circular mr-2"
                            alt="user">
                        Pink
                    </li>
                    <li class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">
                        <img src="https://via.placeholder.com/400x150" class=" w-7 h-7 rounded-circular mr-2"
                            alt="user">
                        Thor
                    </li>
                </ul>

            </div>

            <div class="group w-72 h-24">
                <div class="leading-6 text-neutral-4
                        group-hover:text-primary
                        group-focus:text-primary
                        disabled:text-neutral-5 disabled:bg-white">
                    Cidade
                </div>
                <select class="mt-2 w-72 h-12 border border-gray-400 rounded-top relative leading-6 text-neutral-4
                        group-hover:border-primary group-hover:text-primary
                        group-focus:border-primary
                        focus:outline-none px-4
                        disabled:border-neutral-5">
                    <option class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">
                        <img src="https://via.placeholder.com/400x150" class=" w-7 h-7 rounded-circular mr-2" alt="">
                        Selecione
                    </option>
                    <option class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">Indiana</option>
                    <option class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">Michigan</option>
                    <option class=" p-4 text-neutral-4 hover:bg-neutral-8 duration-75 flex">Ohio</option>
                </select>
            </div>
        </div>



    </section>

    @livewireScripts
</body>

</html>
