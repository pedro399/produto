<div>
    <button type="{{ $type }}" class="flex items-center justify-center bg-{{ $bgColor }} py-2 px-4
        rounded-pill leading-3 font-semibold text-{{ $fontSize }} text-neutral-8 w-full
        tracking-wider shadow-1
        hover:bg-{{ $bgHover }}
        focus:outline-none
        disabled:bg-primary-6 disabled:pointer-events-none" {{ $disabled }}>
        {{ $content }}
        {{-- <img id="img" src="img/{{ $icon }}.svg" class="ml-1 w-5 h-5 "> --}}
    </button>
</div>
