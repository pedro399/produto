<div class="grid grid-cols-2 gap-5">
    <div class="rounded-md p-5 text-white font-semibold w-48 bg-success-3">
        <label class="">
            Visitas virtuais
        </label>
        <br>
        <label class=" text-h3">
            202
        </label>
    </div>

    <div class="rounded-md p-5 text-white font-semibold w-48 bg-primary">
        <label class="">
            Visitas virtuais
        </label>
        <br>
        <label class=" text-h3">
            202
        </label>
    </div>

    <div class="rounded-md p-5 text-white font-semibold w-48 bg-error-2">
        <label class="">
            Visitas virtuais
        </label>
        <br>
        <label class=" text-h3">
            202
        </label>
    </div>
</div>
