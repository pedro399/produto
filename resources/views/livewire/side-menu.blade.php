    <div class="font-poppins fixed flex flex-col gap-y-20 pt-10 max-w-xs h-full shadow-4 rounded-md p-6">

        <div class="flex flex-col gap-y-2 items-center justify-center">
            <img src="../img/user-black.svg" alt="">
            <div class="flex gap-x-2">
                <img src="../img/star.svg" alt="">
                <img src="../img/star.svg" alt="">
                <img src="../img/star.svg" alt="">
                <img src="../img/star.svg" alt="">
                <img src="../img/star.svg" alt="">
            </div>
            <p class=" text-xl">Lorem ipson</p>
            <p class=" text-sm">Lorem ipson</p>
        </div>

        <ul class="flex flex-col gap-y-2 items-center justify-center mb-20">
            <li class="cursor-pointer p-4 bg-neutral-8 rounded w-56 flex border-l-4 border-neutral-1">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                </svg>Imóveis
            </li>
            <li class="cursor-pointer p-4 hover:bg-neutral-8 rounded w-56 ">
                <a href="/corretores/visitas" class="flex">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1" />
                    </svg>Visitas
                </a>
            </li>
            <li class="cursor-pointer p-4 hover:bg-neutral-8 rounded w-56 flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z" />
                </svg>Indicações
            </li>
            <li class="relative cursor-pointer p-4 hover:bg-neutral-8 rounded w-56 flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z" />
                </svg>
                Leads
                <div class="absolute right-0 text-Caption2 bg-error-2 rounded-full w-5 h-5 text-white flex justify-center items-center font-semibold">
                    2
                </div>
            </li>
            <li class="cursor-pointer p-4 hover:bg-neutral-8 rounded-md w-56 flex">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-4" fill="none" viewBox="0 0 24 24"
                    stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        d="M8.228 9c.549-1.165 2.03-2 3.772-2 2.21 0 4 1.343 4 3 0 1.4-1.278 2.575-3.006 2.907-.542.104-.994.54-.994 1.093m0 3h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>Ajuda
            </li>
        </ul>

        <div class=" mx-auto">
            <a href="#" class="bg-primary py-3 px-4 rounded-full text-h4 text-white">
                Alternar mapa
            </a>
        </div>
    </div>
