<div>

    <ul class="flex gap-nano mb-2">
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Valor</li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Dormitórios</li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Vagas</li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Área Privativa</li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Bairros </li>
                    <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Tipos</li>
    </ul>
    <ul class="flex gap-nano">
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Tipos</li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Aceita Dação </li>
        <li
            class="rounded-full px-4 py-2 border-2 border-neutral-4 text-neutral-4 text-sm whitespace-nowrap cursor-pointer hover:bg-neutral-6">
            Em Condomínio</li>

        <li
            class="rounded-full px-4 py-2 text-white bg-primary text-sm whitespace-nowrap cursor-pointer hover:bg-primary-3 leading-4">
            Avançados</li>

    </ul>

</div>
