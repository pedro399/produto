<x-html title="First One" class="font-poppins">

    <x-slot name="head">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
            rel="stylesheet">
    </x-slot>

    <div class="px-20 py-4">
        <h1 class="text-h1 font-SemiBold">Blade Ui Kit</h1>

        <x-form action="#" x-data="{toggle: true}">
            <div class="grid gril-cols-1 my-4">
                <x-label for="Nome_completo"/>
                <x-input name="id_imovel" placeholder="placeholder" />
            </div>

            <div class="grid gril-cols-1 my-4">
                <x-label for="Email" />
                <x-input name="id_imovel" placeholder="placeholder" />
            </div>

            <div class="grid gril-cols-1 my-4">
                <x-label for="Telefone" />
                <x-input name="id_imovel" placeholder="placeholder" />
            </div>

            <x-form-button action="#" size="Xxxs">
                Agendar visita
            </x-form-button>
        </x-form>
    </div>

</x-html>
