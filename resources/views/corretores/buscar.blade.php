<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Buscar Imóveis</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    @livewireStyles
</head>

<body class=" font-poppins text-neutral-1">

    <div class="container flex gap-x-2">

        {{-- MENU LATERAL --}}
        <livewire:side-menu />

        {{-- FILTROS + IMOVEIS --}}
        <div x-data="show_mapa: false" class=" ml-72 font-poppins shadow-4 rounded-md p-4">

            {{-- <div class=" mx-auto" @click="show_mapa=true">
                <a href="#" class="bg-primary py-3 px-4 rounded-full text-h4 text-white">
                    Alternar mapa
                </a>
            </div>

            <div x-show="show_mapa">
                TESTE
            </div> --}}

            {{-- filtro --}}
            <livewire:filters />

            {{-- imoveis --}}
            <div class=" my-6">
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
                <livewire:imovel-card />
            </div>
        </div>

        {{-- DASHBOARD --}}
        <div class="font-poppins fixed bg-white pt-10 h-full shadow-4 rounded-md p-6" style="margin-left: 65rem">
            <livewire:dashboard-card />

            <hr class="my-3">

            <livewire:dashboard-grafic />

            <hr class="my-3">

            <livewire:dashboard-messages />
            <livewire:dashboard-messages />
        </div>


    </div>
    <script src="{{ asset('js/app.js') }}"></script>
    @livewireScripts
</body>

</html>
