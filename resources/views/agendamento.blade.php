<x-html title="Agendamento de visita" class="font-poppins">

    <x-slot name="head">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
            rel="stylesheet">
    </x-slot>

    <nav class="flex justify-between items-center py-1 px-8 text-neutral-2 font-medium shadow-1">
        <a href="/anuncio" class="flex items-center">
            <img src="img/back.svg" alt="" class="p-3">
            <label class="py-2">Voltar</label>
        </a>
    </nav>

    <section class="container mx-auto px-48 py-8 grid grid-cols-3">
        <div class="col-span-2">
            <label class=" font-semibold text-Lg text-neutral-1 block mb-8">Agende sua visita</label>

            <x-form action="#">
                <div class="flex items-center gap-3">
                    <img src="img/user.svg" class="">
                    <label class=" font-semibold text-base text-neutral-1">Informe seus dados</label>
                </div>

                <div class="grid gril-cols-1 my-4">
                    <x-label for="Nome_completo" />
                    <x-input name="id_imovel" placeholder="placeholder" />
                </div>

                <div class="grid gril-cols-1 my-4">
                    <x-label for="Email" />
                    <x-input name="id_imovel" placeholder="placeholder" />
                </div>

                <div class="grid gril-cols-1 my-4">
                    <x-label for="Telefone" />
                    <x-input name="id_imovel" placeholder="placeholder" />
                </div>
            </x-form>

            <x-form action="#" class="mt-12">
                <div class="flex items-center gap-3 mb-4">
                    <img src="img/user.svg" class="">
                    <label class=" font-semibold text-base text-neutral-1">Data</label>
                </div>
                <label class="text-neutral-3 leading-4 text-base">Selecione o dia para fazer a visita</label>

                <div class=" max-w-sm my-4">
                    <ul class="grid grid-cols-3 gap-4 text-sm text-neutral-4">
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">Qua 30/06</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">Qua 30/06</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">Qua 30/06</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">Qua 30/06</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">Qua 30/06</li>
                    </ul>
                </div>
            </x-form>

            <x-form action="#" class="mt-12">
                <div class="flex items-center gap-3 mb-4">
                    <img src="img/user.svg" class="">
                    <label class=" font-semibold text-base text-neutral-1">Hora</label>
                </div>
                <label class="text-neutral-3 leading-4 text-base">Utilize a opção de horário flexível caso precise
                    realizar
                    a visita em um horário quebrado.</label>

                <div class=" max-w-sm my-4">
                    <ul class="grid grid-cols-4 gap-8 text-sm text-neutral-4">
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">8:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">8:30</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">9:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">9:30</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">10:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">10:30</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">8:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">8:30</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">9:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">9:30</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">10:00</li>
                        <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center">10:30</li>
                    </ul>
                </div>
            </x-form>

            <x-form action="#" class="mt-12">
                <div class="flex items-center gap-3 mb-4">
                    <img src="img/user.svg" class="">
                    <label class=" font-semibold text-base text-neutral-1">Detalhes (Opcional)</label>
                </div>
                <label class="text-neutral-3 leading-4 text-base">Se houver, insira no campo abaixo alguma observação
                    que
                    seja importante para a realização da sua visita.</label>

                <div class=" max-w-sm my-4">
                    <textarea cols="50" rows="5" class="border-2 border-neutral-4 rounded-md"></textarea>
                </div>

                <x-form-button action="#" class=" mt-14">
                    Agendar visita
                </x-form-button>
            </x-form>
        </div>
        <div>
            {{-- CARD --}}
            <div class="flex mt-4">
                <div class="w-72 shadow-1 rounded-md m-4">
                    <img src="img/room.png" alt="" class="object-cover h-48 rounded-t-md">
                    <div class=" px-6 py-4">
                        <p class=" font-semibold text-primary text-xl leading-3 mb-2">R$ 1.896.000,00</p>
                        <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                        <p class=" text-neutral-3 leading-4 mb-2">Menino Deus, Porto Alegre</p>
                        <div class="flex justify-between">
                            <label class="flex text-neutral-4">
                                <img src="img/bed.svg" class=" mr-1" alt=""> 2
                            </label>
                            <label class="flex text-neutral-4">
                                <img src="img/vaga.svg" class=" mr-1" alt=""> 1
                            </label>
                            <label class="flex text-neutral-4">
                                <img src="img/area.svg" class=" mr-1" alt=""> 82m²
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            {{-- VALORES --}}
            <div class="row-span-3 py-8 w-72 m-4">
                <div class=" mb-4">
                    <div class=" text-neutral-3 text-Xxxs">Valor da venda</div>
                    <div class=" text-neutral-1 text-4xl font-semibold">R$ 1.280.000,00</div>
                </div>
                <div class="pb-8 grid grid-cols-2 gap-y-4">
                    <div class="grid gap-y-4">
                        <div class=" text-neutral-3 text-Xxxs">Condomínio</div>
                        <div class=" text-neutral-3 text-Xxxs">IPTU</div>
                    </div>
                    <div class="grid gap-y-4 justify-items-end">
                        <div class=" text-neutral-3 text-sm font-semibold">R$ 400,00</div>
                        <div class=" text-neutral-3 text-sm font-semibold">R$ 370,00</div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</x-html>
