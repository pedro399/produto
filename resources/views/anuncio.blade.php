<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Anúncio Imóvel</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700;800;900&display=swap"
        rel="stylesheet">

    <script src="https://unpkg.com/alpinejs@3.2.1/dist/cdn.min.js"></script>

    <style>
        .btn-active {
            color: white !important;
        }

    </style>
    @livewireStyles
</head>

<body class="font-poppins"
    :class="{'overflow-hidden' : open_desc || open_details_cond || open_details_imovel || open_agend || open_galery || open_video_desk || open_visita_desk ||open_planta_desk || mob_360 || mob_video || mob_planta || open_galeria_planta}"
    x-data="{open_galery: false, open_agend: false, open_loc: false, open_share:false, open_galeria_planta: false,
            open_desc: false, open_details_imovel:false, open_details_cond:false, open_agen_conf:false,
            open_video_desk: false, open_visita_desk: false, open_planta_desk: false,
            thumb_360_mob: false, thumb_fotos_mob: true, thumb_video_mob: false, thumb_planta_mob: false,
            mob_360:false, mob_video:false, mob_planta:false,
            thumb_visita_desk: false, thumb_fotos_desk: true, thumb_planta_desk: false, thumb_video_desk: false}">

    <nav class="hidden md:grid w-full h-16 bg-primary grid-cols-12 px-xs py-nano items-center over">
        <div class="col-span-7">
            <img src="{{ asset('img/logo-white.svg') }}" alt="Logo" class="">
        </div>

        <div class=" col-span-3">
            <button type="button" class="flex items-center justify-center bg-white py-2 px-4
                                rounded-pill leading-04 font-semibold text-Xxs text-primary
                                tracking-wider shadow-1 cursor-pointer
                                hover:bg-neutral-5
                                disabled:bg-primary-6 disabled:pointer-events-none">
                Quero anunciar meu imóvel
            </button>
        </div>
        <div class="flex items-center border-l-2 border-solid border-neutral-8 col-span-2">
            <img src="https://via.placeholder.com/150x150" alt="user"
                class="w-sm h-sm rounded-full border-2 border-white ml-4">

            <span class=" text-white font-medium text-base pl-4 ">Carla Fiorini</span>
        </div>
    </nav>

    {{-- VALOR FIXO DESKTOP --}}



    {{-- CORPO --}}
    <div class="container md:px-12" :class="{'hidden' : open_agend || open_galery || open_loc || open_galeria_planta}">

        {{-- CAROUSEL MOBILE --}}
        <div class="block md:hidden relative">
            {{-- thumb_fotos_mob / thumb_360_mob / thumb_video_mob / thumb_planta_mob --}}
            <div x-show="thumb_fotos_mob">
                <a @click="open_galery=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 px-4 py-3
                    bg-neutral-1 bg-opacity-80 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    Ver mais fotos
                    <img src="{{ asset('img/fotos_branca.svg') }}" alt="">
                </a>
                <img src="{{ asset('img/image 30.png') }}" alt="user" class="w-full h-60 object-cover">
            </div>
            <div x-show="thumb_360_mob">
                <a @click="mob_360=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 px-4 py-3
                        bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
                <iframe class="w-full h-60 object-cover"
                    src="https://my.matterport.com/show/?m=pcdqFtYDSaF&amp;utm_source=4&amp;play=1&amp;ts=30&amp;qs=0&amp;lp=1"
                    frameborder="0" allowfullscreen="allowfullscreen"></iframe>

                <div x-show="mob_360" class="absolute inset-0 w-screen h-screen bg-black bg-opacity-80">
                    <div class="flex gap-3 items-center justify-end p-3 cursor-pointer mt-8" @click="mob_360=false">
                        <label class="text-white">Fechar</label>
                        <img src="{{ asset('img/close-white.svg') }}" class="h-6 w-6">
                    </div>
                    <iframe
                        src="https://my.matterport.com/show/?m=pcdqFtYDSaF&amp;utm_source=4&amp;play=1&amp;ts=30&amp;qs=0&amp;lp=1"
                        frameborder="0" allowfullscreen="allowfullscreen"
                        style="width: 100%; height: 90%; margin-top: 1rem;"></iframe>
                </div>
            </div>
            <div x-show="thumb_video_mob">
                <a @click="mob_video=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 px-4 py-3
                    bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
                <iframe class="w-full h-60 object-cover" src="https://www.youtube.com/embed/6Qwi3HL948E"
                    title="YouTube video player" frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>

                <div x-show="mob_video" class="absolute inset-0 w-screen h-screen bg-black bg-opacity-80">
                    <div class="flex gap-3 items-center justify-end p-3 cursor-pointer mt-8" @click="mob_video=false">
                        <label class="text-white">Fechar</label>
                        <img src="{{ asset('img/close-white.svg') }}" class="h-6 w-6">
                    </div>
                    <iframe style="width: 100%; height: 50%; margin-top: 2rem;"
                        src="https://www.youtube.com/embed/6Qwi3HL948E" title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
            </div>
            <div x-show="thumb_planta_mob">
                <a @click="open_galeria_planta=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 px-4 py-3
                    bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
                <img src="{{ asset('img/planta.png') }}" alt="user" class="w-full h-60 object-cover">

            </div>
        </div>

        {{-- breadcrumb --}}
        <div id="thumbs" class="hidden md:flex w-full py-4 justify-between items-center">
            <ul class="flex text-Xxxs text-neutral-5">
                <li class="px-2"><a href="#">Porto Alegre</a></li>
                <li>></li>
                <li class="px-2"><a href="#">Ney da Gama Ahrends</a></li>
                <li>></li>
                <li class="px-2"><a href="#">Ney da Gama Ahrends</a></li>
            </ul>

            <div class="flex items-center">
                <a href="" class="p-3 cursor-pointer">
                    <img src="{{ asset('img/favorite.svg') }}">
                </a>
                <a @click="open_share=true" class="p-3 cursor-pointer">
                    <img src="{{ asset('img/share.svg') }}">
                </a>
            </div>

            <div x-show="open_share"
                class="bg-white px-4 py-3 flex items-center justify-center gap-2 shadow-2 rounded-md absolute top-3 z-50" style="right: 31%;">
                <label class="block whitespace-nowrap overflow-hidden text-neutral-3 max-w-xs leading-4" id="copiar">
                    https://agenciou.com.br/portal/imovel/apartamento/rs/porto-alegre/cristal/8062#
                </label>

                <a @click="open_share=false" class="flex items-center justify-center py-3 px-4
                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                    tracking-wider border-2 border-primary ring-inset cursor-pointer
                    hover:text-primary-3 hover:border-primary-3
                    focus:outline-none focus:bg-neutral-7
                    disabled:bg-primary-6 disabled:text-primary-6
                    disabled:pointer-events-none">
                    Copiar
                </a>
            </div>
        </div>

        {{-- CAROUSEL DESKTOP --}}
        <div>
            <div x-show="thumb_fotos_desk"
                class="hidden md:grid grid-cols-4 gap-2 mx-auto rounded-md items-center xl:max-w-6xl">
                <div class="row-span-2 col-span-2">
                    <img width="600" height="408" class=" rounded-l-md" src="{{ asset('img/image 30.png') }}" alt="">
                </div>
                <div class="">
                    <img width="300" height="200" src="{{ asset('img/image 30.png') }}" alt="">
                </div>
                <div class="">
                    <img width="300" height="200" class="rounded-tr-md" src="{{ asset('img/image 30.png') }}" alt="">
                </div>
                <div class="">
                    <img width="300" height="200" src="{{ asset('img/image 30.png') }}" alt="">
                </div>
                <div class="flex items-center justify-center relative z-ne">
                    <a @click="open_galery=true"
                        class=" z-10 cursor-pointer flex items-center justify-center gap-2 absolute bottom-4 right-6 px-4 py-3 bg-neutral-1 bg-opacity-80 rounded-lg text-white text-Xxxs font-semibold leading-3 ">
                        Ver mais fotos
                        <img src="{{ asset('img/fotos_branca.svg') }}" alt="">
                    </a>
                    <img width="300" height="200" class="rounded-br-md" src="{{ asset('img/image 30.png') }}" alt="">
                </div>
            </div>

            <div x-show="thumb_visita_desk" class="hidden md:block relative">
                {{-- <img src="{{ asset('img/visita virtual.png') }}" class="w-full h-96 object-cover rounded-md"> --}}
                <div class="w-full h-96 object-cover rounded-md flex items-center justify-center bg-black">
                    <iframe
                        src="https://my.matterport.com/show/?m=pcdqFtYDSaF&amp;utm_source=4&amp;play=1&amp;ts=30&amp;qs=0&amp;lp=1"
                        frameborder="0" allowfullscreen="allowfullscreen"
                        style="width: 100%; height: 24rem; border-radius: 12px;"></iframe>
                </div>
                <a @click="open_visita_desk=true" class="cursor-pointer flex items-center justify-center gap-2 absolute bottom-4 right-6 p-3
                        bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
            </div>

            <div x-show="open_visita_desk" class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed top-0 left-0">
                <div class=" max-w-4xl mt-4 m-auto">
                    <div class="flex gap-3 items-center justify-end p-3 cursor-pointer" @click="open_visita_desk=false">
                        <label class="text-white">Fechar</label>
                        <img src="{{ asset('img/close-white.svg') }}" class="h-6 w-6">
                    </div>
                    <div class=" bg-neutral-1">
                        <iframe
                            src="https://my.matterport.com/show/?m=pcdqFtYDSaF&amp;utm_source=4&amp;play=1&amp;ts=30&amp;qs=0&amp;lp=1"
                            frameborder="0" allowfullscreen="allowfullscreen"
                            style="width: 100%; height: 35rem;"></iframe>
                    </div>
                </div>
            </div>

            <div x-show="thumb_planta_desk" class="hidden md:block relative">
                <img src="{{ asset('img/planta baixa.png') }}" class="w-full h-96 object-cover rounded-md">
                <a @click="open_galeria_planta=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 p-3
                    bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
            </div>

            <div x-show="open_planta_desk" class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed top-0 left-0">
                <div class=" max-w-4xl mt-4 m-auto">
                    <div class="flex gap-3 items-center justify-end p-3 cursor-pointer" @click="open_planta_desk=false">
                        <label class="text-white">Fechar</label>
                        <img src="{{ asset('img/close-white.svg') }}" class="h-6 w-6">
                    </div>
                    <div class=" bg-neutral-1">
                        <img src="{{ asset('img/planta baixa2.png') }}">
                    </div>
                </div>
            </div>

            <div x-show="thumb_video_desk" class="hidden md:block relative">
                {{-- <img src="{{ asset('img/video desk.png') }}" class="w-full h-96 object-cover rounded-md"> --}}
                <div class="w-full h-96 object-cover rounded-md flex items-center justify-center bg-black">
                    <iframe width="900" height="384" src="https://www.youtube.com/embed/6Qwi3HL948E"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <a @click="open_video_desk=true" class="flex items-center justify-center gap-2 absolute bottom-4 right-6 p-3
                    bg-primary-3 rounded-lg text-white text-Xxxs font-semibold leading-3 cursor-pointer">
                    <img src="{{ asset('img/size.svg') }}" alt="">
                </a>
            </div>

            <div x-show="open_video_desk" class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed top-0 left-0">
                <div class=" max-w-4xl mt-4 m-auto">
                    <div class="flex gap-3 items-center justify-end p-3 cursor-pointer" @click="open_video_desk=false">
                        <label class="text-white">Fechar</label>
                        <img src="{{ asset('img/close-white.svg') }}" class="h-6 w-6">
                    </div>
                    <div class=" bg-neutral-1">
                        <div>
                            <iframe width="900" height="500" src="https://www.youtube.com/embed/6Qwi3HL948E"
                                title="YouTube video player" frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- PRATICAMENTE TUDO --}}
        <div class="p-4 md:px-0">

            <div class="md:grid md:grid-cols-4">
                <div class="col-span-3">
                    {{-- FOTOS - VIDEO - TOUR --}}
                    <div class="pb-4 md:pb-10 md:pt-8 flex gap-3 md:gap-4">

                        {{-- FOTOS (desk - mob) --}}
                        <button
                            @click="thumb_fotos_mob=true;thumb_360_mob=false;thumb_video_mob=false;thumb_planta_mob=false;thumb_fotos_desk=true;thumb_visita_desk=false;thumb_planta_desk=false;thumb_video_desk=false"
                            type="button" class="flex items-center justify-center py-3 md:py-2 px-4
                           rounded-pill leading-3 font-semibold text-Xxxs md:text-Xxs text-primary
                           tracking-wider border-2 border-primary ring-inset cursor-pointer
                           focus:outline-none
                           disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_fotos_mob || thumb_fotos_desk}">
                            Fotos
                            {{-- <img src="img/fotos.svg" alt="" class="hidden md:block ml-2"> --}}

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 ml-2 hidden md:block" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                            </svg>
                        </button>

                        {{-- 360 (mob) --}}
                        <button
                            @click="thumb_fotos_mob=false;thumb_fotos_desk=false;thumb_360_mob=true;thumb_video_mob=false;thumb_video_desk=false;thumb_planta_mob=false;"
                            type="button" class="md:hidden flex items-center justify-center py-3 px-4
                           rounded-pill leading-3 font-semibold text-Xxxs text-primary
                           tracking-wider border-2 border-primary ring-inset
                           focus:outline-none focus:bg-primary focus:text-white focus:shadow-1
                           disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_360_mob}">
                            360
                        </button>

                        {{-- VIDEO (desk - mob) --}}
                        <button
                            @click="thumb_fotos_mob=false;thumb_360_mob=false;thumb_video_mob=true;thumb_planta_mob=false;thumb_fotos_desk=false;thumb_visita_desk=false;thumb_planta_desk=false;thumb_video_desk=true"
                            type="button" class="flex items-center justify-center py-3 md:py-2 px-4
                            rounded-pill leading-3 font-semibold text-Xxxs md:text-Xxs text-primary
                            tracking-wider border-2 border-primary ring-inset cursor-pointer
                            focus:outline-none focus:bg-primary focus:text-white focus:shadow-1
                            disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_video_mob || thumb_video_desk}">
                            Vídeo
                            {{-- <img src="img/video_primary.svg" alt="" class="hidden md:block ml-2"> --}}

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 ml-2 hidden md:block"
                                viewBox="0 0 20 20" fill="currentColor">
                                <path
                                    d="M2 6a2 2 0 012-2h6a2 2 0 012 2v8a2 2 0 01-2 2H4a2 2 0 01-2-2V6zM14.553 7.106A1 1 0 0014 8v4a1 1 0 00.553.894l2 1A1 1 0 0018 13V7a1 1 0 00-1.447-.894l-2 1z" />
                            </svg>
                        </button>

                        {{-- VISITA VIRTUAL (desk) --}}
                        <button
                            @click="thumb_fotos_desk=false;thumb_fotos_mob=false;thumb_visita_desk=true;thumb_planta_desk=false;thumb_video_desk=false;thumb_video_mob=false"
                            type="button" class="hidden md:flex items-center justify-center py-2 px-4
                            rounded-pill leading-3 font-semibold text-Xxs text-primary
                            tracking-wider border-2 border-primary ring-inset cursor-pointer
                            focus:outline-none focus:bg-primary focus:text-white focus:shadow-1
                            disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_visita_desk}">
                            Visita virtual
                            {{-- <img src="img/visita_virtual.svg" alt="" class="ml-2"> --}}

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 ml-2 hidden md:block" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9" />
                            </svg>
                        </button>

                        {{-- PLANTA BAIXA (mob) --}}
                        <button
                            @click="thumb_fotos_mob=false;thumb_fotos_desk=false;thumb_360_mob=false;thumb_video_mob=false;thumb_video_desk=false;thumb_planta_mob=true;"
                            type="button" class="md:hidden flex items-center justify-center py-3 px-4
                             rounded-pill leading-3 font-semibold text-Xxxs text-primary
                             tracking-wider border-2 border-primary ring-inset
                             focus:outline-none focus:bg-primary focus:text-white focus:shadow-1
                             disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_planta_mob}">
                            Planta
                        </button>

                        {{-- PLANTA BAIXA (desk) --}}
                        <button
                            @click="thumb_fotos_desk=false;thumb_fotos_mob=false;thumb_visita_desk=false;thumb_planta_desk=true;thumb_video_desk=false;thumb_video_mob=false"
                            type="button" class="hidden md:flex items-center justify-center py-2 px-4
                                    rounded-pill leading-3 font-semibold text-Xxs text-primary
                                    tracking-wider border-2 border-primary ring-inset cursor-pointer
                                    focus:outline-none focus:bg-primary focus:text-white focus:shadow-1
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none"
                            :class="{'btn-active bg-primary shadow-1 outline-none' : thumb_planta_desk}">
                            Planta baixa
                            <template x-if="!thumb_planta_desk">
                                <img src="{{ asset('img/planta_baixa_primary.svg') }}" alt=""
                                    class="ml-2 hidden md:block">
                            </template>
                            <template x-if="thumb_planta_desk">
                                <img src="{{ asset('img/planta_baixa_white.svg') }}" alt=""
                                    class="ml-2 hidden md:block">
                            </template>
                        </button>

                    </div>
                    {{-- ENDEREÇO --}}
                    <div class="pb-8 md:pb-10">
                        <label class="text-h3 md:text-h1 font-semibold text-neutral-1">Imóvel na Vila Assunção</label>
                        <p class="text-Xxs text-neutral-3 my-2 leading-4">
                            Rua Ney da Gama Ahrends, Alto Petrópolis, Porto Alegre
                        </p>
                        <button @click="open_loc=true" type="button" class="flex items-center justify-center pb-2 pt-4 md:pt-6
                                rounded-pill leading-3 font-semibold text-Xxs text-primary
                                tracking-wider cursor-pointer
                                hover:text-primary-3
                                focus:outline-none focus:bg-neutral-7
                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                            Ver localização
                            <img src="{{ asset('img/localizacao.svg') }}" alt="" class=" ml-2">
                        </button>
                    </div>
                    <hr class="pb-8 md:pb-10 max-w-3xl">
                    {{-- DETALHES --}}
                    <div class="pb-8 grid grid-cols-2 gap-y-6 md:grid-cols-3 md:pb-10 md:max-w-3xl"
                        id="caracteristicas">
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/dorm.svg') }}" alt="" class=" mr-2"> 2
                            Dormitórios
                        </label>
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/sol.svg') }}" alt="" class=" mr-2"> Sol Leste
                            -
                            Oeste
                        </label>
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/banheiro.svg') }}" alt="" class=" mr-2"> 2
                            Banheiros
                        </label>
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/imovel.svg') }}" alt="" class=" mr-2"> 3º
                            Andar
                        </label>
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/vaga.svg') }}" alt="" class=" mr-2"> 2 Vagas
                        </label>
                        <label class="flex text-neutral-3 leading-4 text-Xxs">
                            <img src="{{ asset('img/area.svg') }}" alt="" class=" mr-2"> 84 m²
                        </label>
                    </div>
                    <hr class="max-w-3xl md:hidden">
                </div>
                {{-- VALORES --}}
                <div class="row-span-3 py-8">
                    <div class="pb-8 grid grid-cols-2 gap-y-2">
                        <div class="md:col-span-2">
                            <div class=" text-neutral-3 text-Xxxs">Valor da venda</div>
                        </div>
                        <div class="md:col-span-2">
                            <div class=" text-neutral-1 text-h3 md:text-h1 font-semibold">R$ 1.280.000,00</div>
                        </div>
                        <div class="grid gap-y-4">
                            <div class=" text-neutral-3 text-Xxxs">Condomínio</div>
                            <div class=" text-neutral-3 text-Xxxs">IPTU</div>
                        </div>
                        <div class="grid gap-y-4 justify-items-end">
                            <div class=" text-neutral-3 text-Xxs md:text-sm font-semibold">R$ 400,00</div>
                            <div class=" text-neutral-3 text-Xxs md:text-sm font-semibold">R$ 370,00</div>
                        </div>
                    </div>

                    <a @click="open_agend=true" class="w-full flex items-center justify-center bg-primary py-3 px-4
                            rounded-pill leading-3 font-semibold text-Xxs md:text-Sm text-neutral-8
                            tracking-wider shadow-1
                            mb-3 cursor-pointer
                            hover:bg-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:pointer-events-none">
                        Agendar visita
                    </a>
                    <a type="button" class="w-full flex items-center justify-center py-3 px-4
                            rounded-pill leading-3 font-semibold text-Xxs md:text-Sm text-primary
                            tracking-wider border-2 border-primary ring-inset cursor-pointer
                            hover:text-primary-3 hover:border-primary-3
                            focus:outline-none focus:bg-neutral-7
                            disabled:bg-primary-6 disabled:text-primary-6
                            disabled:pointer-events-none">
                        Fazer proposta
                    </a>
                    <a type="button" class="w-full hidden md:flex items-center justify-center py-3 px-4
                            rounded-pill leading-3 font-semibold text-Xxxs text-primary
                            tracking-wider ring-inset cursor-pointer
                            hover:text-primary-3 hover:border-primary-3
                            focus:outline-none focus:bg-neutral-7
                            disabled:bg-primary-6 disabled:text-primary-6
                            disabled:pointer-events-none">
                        Tire suas dúvidas com a gente >
                    </a>
                </div>
                <hr class=" pb-8 pt-8 md:hidden">
            </div>
        </div>

    </div>

    <nav class="sticky top-0 z-50"
        :class="{'hidden' : open_agend || open_galery || open_loc || open_galeria_planta || open_desc || open_details_imovel || open_details_cond || open_visita_desk || open_video_desk}">

        <div
            class="hidden w-full bg-white px-28 pt-3 pb-4 md:flex justify-between items-center shadow-4 absolute top-0 mb-4 z-50">
            <div>
                <a href="#thumbs"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Fotos</a>
                <a href="#thumbs"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Tour
                    virtual</a>
                <a href="#caracteristicas"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Características</a>
                <a href="#thumbs"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Vídeo</a>
                <a href="#localiza"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Localização</a>
                <a href="#thumbs"
                    class="cursor-pointer text-primary text-Xxs font-medium leading-4 p-2 rounded-sm hover:bg-neutral-8">Planta</a>
            </div>

            <div class="flex justify-center items-center gap-4">
                <div>
                    <label class="block text-neutral-3 text-sm leading-4">Valor de venda</label>
                    <label class="block text-neutral-1 text-base leading-3 font-semibold">R$
                        1.280.000,00</label>
                </div>

                <a type="button" @click="open_agend=true" class="flex items-center justify-center bg-primary py-3 px-4
                                            rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                                            tracking-wider shadow-1 cursor-pointer
                                            hover:bg-primary-3
                                            focus:outline-none
                                            disabled:bg-primary-6 disabled:pointer-events-none">

                    Agendar visita
                </a>
            </div>
        </div>
    </nav>

    {{-- CORPO CONTINUAÇÃO --}}
    <div class="container md:px-12 md:mt-11"
        :class="{'hidden' : open_agend || open_galery || open_loc || open_galeria_planta}">


        <div class="p-4 md:px-0">

            {{-- DESCRIÇÃO IMOVEL --}}
            <div class="pb-8 md:max-w-3xl md: pt-20 md:pb-14">
                <div class="flex pb-4">
                    <img src="{{ asset('img/book.svg') }}" alt="book" class=" mr-4">
                    <label class=" text-neutral-1 text-h4 md:text-h3 leading-3 font-semibold">Descrição do
                        imóvel</label>
                </div>

                <div class="flex justify-between md:justify-start pb-4">
                    <div class="flex gap-2 md:mr-8">
                        <img src="{{ asset('img/check_details.svg') }}" alt="">
                        <label class="md:text-Xs text-Xxxs text-neutral-3">Imóvel financiável</label>
                    </div>
                    <div class="flex gap-2">
                        <img src="{{ asset('img/nocheck_details.svg') }}" alt="">
                        <label class="md:text-Xs text-Xxxs text-neutral-3">Não aceita permuta</label>
                    </div>
                </div>

                <div class="mb-2 line-clamp-3 text-neutral-3 text-Xxs">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                    labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore
                </div>

                <div>

                    <button @click="open_desc=true" class="flex items-center justify-center
                                    rounded-pill leading-3 font-semibold text-Xxxs md:text-Xxs text-primary
                                    tracking-wider cursor-pointer
                                    hover:text-primary-3
                                    focus:outline-none
                                    disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Ler mais
                        <img src="{{ asset('img/right_arrow.svg') }}" class="p-1">
                    </button>

                    {{-- MODAL DESCRIÇÃO IMOVEL --}}
                    <div x-show="open_desc"
                        class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed top-0 left-0">

                        <div @click.away="open_desc = false" class="max-w-4xl h-5/6 my-8 bg-white px-8 py-6 rounded-md
                                                                    shadow-3 grid gap-y-6 mx-xxxs md:mx-auto">
                            <div class="flex justify-between items-center">
                                <div class="flex gap-2">
                                    <img src="{{ asset('img/book.svg') }}" alt="">
                                    <label class=" font-semibold text-xl leading-3">Sobre o imóvel</label>
                                </div>
                                <div>
                                    <a class="cursor-pointer">
                                        <img @click="open_desc=false" class="p-1" src="{{ asset('img/close.svg') }}"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="overflow-auto">

                                <div class="md:flex md:gap-8">
                                    <div class="flex gap-2">
                                        <img src="{{ asset('img/check_details.svg') }}" alt="">
                                        <label class=" text-Xxxs text-neutral-3">Imóvel financiável</label>
                                    </div>
                                    <div class="flex gap-2">
                                        <img src="{{ asset('img/nocheck_details.svg') }}" alt="">
                                        <label class=" text-Xxxs text-neutral-3">Não aceita permuta</label>
                                    </div>
                                </div>
                                <hr class=" my-4">
                                <div class=" text-neutral-3 text-Xxs grid gap-y-4">
                                    <p>Apartamento impecável de 83,9m² privativos, com 2 suítes, sendo uma delas suíte
                                        master com
                                        closet.
                                        Há um total de 3 banheiros com ventilação natural, sendo 1 lavabo, 2 vagas de
                                        garagem, posição
                                        solar
                                        extremamente privilegiada.
                                        Este imóvel agrega muito conforto em cada peça. Na sala o living com lareira
                                        está
                                        integrado com
                                        a
                                        churrasqueira privativa e a cozinha. A mobília planejada por arquiteta entrega
                                        sofisticação e
                                        bem-estar. Ficam no imóvel: móveis planejados de ambas suítes, cozinha, sala,
                                        banheiros, balcão
                                        de
                                        vidro lavabo e porta de segurança.
                                    </p>
                                    <p>O condomínio possui salão de festas mobiliado. Além, do destaque para localização
                                        privilegiada,
                                        muito próxima ao comércio da Av. Wenceslau Escobar com a tranquilidade de uma
                                        rua
                                        residencial
                                        tranquila na zona sul da capital. Muito próximo ao shopping Paseo e poucos
                                        minutos
                                        do Barra
                                        Shopping
                                        Sul.
                                    </p>
                                    <p>A Tristeza inseriu-se no caráter de urbanização e modernização da cidade na
                                        primeira
                                        metade do
                                        século XX, recebendo o Jockey Club do Rio Grande do Sul, o Estaleiro Só e
                                        diversas
                                        fábricas como
                                        a
                                        Termolar. É um bairro residencial, tranquilo e arborizado na Zona Sul de Porto
                                        Alegre e que
                                        também
                                        oferece amplo comércio e serviços, além de belas paisagens, já que se encontra
                                        às
                                        margens do
                                        Guaíba.
                                        No bairro também localizam-se o Barra Shopping Sul, maior complexo comercial da
                                        cidade, o
                                        Cristal
                                        Tênis Center, o Iate Clube Guaíba e a Fundação Iberê Camargo e seu belo projeto
                                        arquitetônico de
                                        autoria de Álvaro Siza e também o Pontal que vai reunir um parque, um shopping
                                        center, um hotel
                                        padrão internacional, um centro de eventos completo, uma torre multiuso e o hub
                                        de
                                        saúde Pontal
                                        Clínicas, um novo Centro Comercial à beira do Guaíba.
                                    </p>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <hr class=" pb-8 md:max-w-3xl md:pb-0">

            {{-- DETALHE IMOVEL --}}
            <div class="pb-8 md:max-w-3xl md:py-14">
                <div class="flex pb-4">
                    <img src="{{ asset('img/door.svg') }}" alt="book" class=" mr-4">
                    <label class=" text-neutral-1 text-h4 md:text-h3 leading-3 font-semibold">Detalhes do imóvel</label>
                </div>

                <div class="mb-2 text-neutral-3 text-Xxs">
                    <ul class="flex flex-wrap gap-y-1">
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Churrasqueira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Água quente</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Água quente</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Churrasqueira privativa</li>
                    </ul>
                </div>

                <div>

                    <button @click="open_details_imovel=true" type="button" class="flex items-center justify-center
                                rounded-pill leading-3 font-semibold text-Xxxs md:text-Xxs text-primary
                                tracking-wider cursor-pointer
                                hover:text-primary-3
                                focus:outline-none
                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Ver detalhes do imóvel
                        <img src="{{ asset('img/right_arrow.svg') }}" class="p-1">
                    </button>

                    <div x-show="open_details_imovel" class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed
                                                            top-0 left-0 flex items-center justify-center ">

                        <div @click.away="open_details_imovel = false"
                            class="mx-auto my-8 bg-white px-8 py-6 rounded-md shadow-3 grid gap-y-6">

                            <div class="flex justify-between items-center">
                                <div class="flex gap-2">
                                    <img src="{{ asset('img/book.svg') }}" alt="">
                                    <label class=" font-semibold text-xl leading-3">Detalhes do imóvel</label>
                                </div>
                                <div>
                                    <a class="cursor-pointer">
                                        <img @click="open_details_imovel=false" class="p-1"
                                            src="{{ asset('img/close.svg') }}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="grid md:grid-cols-2 grid-cols-1 gap-y-4 max-h-96 md:max-h-full overflow-auto">
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Área de serviço</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Armários cozinha</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                            </div>
                            <div class=" text-neutral-3 text-Xxs grid gap-y-4 overflow-auto">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class=" pb-8 md:max-w-3xl md:pb-0">

            {{-- DETALHES CONDOMINIO --}}
            <div class="pb-8 md:max-w-3xl md:py-14">
                <div class="flex pb-4">
                    <img src="{{ asset('img/build.svg') }}" alt="book" class=" mr-4">
                    <label class=" text-neutral-1 text-h4 md:text-h3 leading-3 font-semibold">Detalhes do
                        condomínio</label>
                </div>

                <div class="mb-2 line-clamp-3 text-neutral-3 text-Xxs">
                    <ul class="flex flex-wrap gap-y-1">
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Churrasqueira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Água quente</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Cozinha americana</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Água quente</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Lareira privativa</li>
                        <li><img src="{{ asset('img/dot.svg') }}" alt=""></li>
                        <li>Churrasqueira privativa</li>
                    </ul>
                </div>

                <div id="localiza">

                    <button @click="open_details_cond=true" type="button" class="flex items-center justify-center
                            rounded-pill leading-3 font-semibold text-Xxxs md:text-Xxs text-primary
                            tracking-wider cursor-pointer
                            hover:text-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                        Ver detalhes do condomínio
                        <img src="{{ asset('img/right_arrow.svg') }}" class="p-1">
                    </button>

                    <div x-show="open_details_cond" class="bg-neutral-1 bg-opacity-70 h-screen w-screen py-8 fixed
                                                        top-0 left-0 flex items-center justify-center z-50">

                        <div @click.away="open_details_cond = false"
                            class="mx-auto my-8 bg-white px-8 py-6 rounded-md shadow-3 grid gap-y-6">

                            <div class="flex justify-between items-center">
                                <div class="flex gap-2">
                                    <img src="{{ asset('img/book.svg') }}" alt="">
                                    <label class=" font-semibold text-xl leading-3">Detalhes do imóvel</label>
                                </div>
                                <div>
                                    <a class="cursor-pointer">
                                        <img @click="open_details_cond=false" class="p-1"
                                            src="{{ asset('img/close.svg') }}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="grid md:grid-cols-2 grid-cols-1 gap-y-4 max-h-96 md:max-h-full overflow-auto">
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/check_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Área de serviço</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Armários cozinha</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Cozinha americana</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Imóvel financiável</label>
                                </div>
                                <div class="flex gap-2 border-b-2 border-neutral-6 pb-4 md:mr-8 mr-2">
                                    <img src="{{ asset('img/nocheck_details.svg') }}" class="w-6">
                                    <label class=" text-Xxs text-neutral-3">Churrasqueira privativa</label>
                                </div>
                            </div>
                            <div class=" text-neutral-3 text-Xxs grid gap-y-4 overflow-auto">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class=" pb-8 md:max-w-3xl md:pb-0">

            {{-- LOCALIZAÇÃO --}}
            <div class="pb-8 md:py-14">
                <div class="grid grid-cols-8 pb-4 gap-y-4">
                    <div class="col-span-6 flex">
                        <img src="{{ asset('img/localizacao.svg') }}" alt="book" class="mr-4">
                        <label class=" text-neutral-1 text-h4 md:text-h3 leading-3 font-semibold">Localização</label>
                    </div>
                    <label class=" text-neutral-3 text-base leading-4 col-span-8 md:hidden">
                        Clique na imagem para conhecer melhor a região do imóvel e
                        poder alternar entre o mapa e a visão da rua.</label>
                    <label class=" text-neutral-3 text-base leading-4 col-span-8 hidden md:block">
                        Conheça a região onde o imóvel se encontra e sua visão da rua.</label>
                </div>

                <div x-data="{rua: false, mapa: true}" class="hidden md:block md:max-w-3xl md:relative">

                    <div x-show="mapa">
                        <img src="{{ asset('img/view.png') }}"
                            class="object-cover w-full h-96 rounded-md mb-6 z-10" />

                        <a type="button" @click="mapa=false;rua=true" class="flex items-center justify-center gap-1 bg-primary py-3 px-4
                            rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8 absolute top-6 right-8 z-40
                            tracking-wider shadow-1 mb-3 cursor-pointer
                            hover:bg-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:pointer-events-none">
                            Visão da rua
                            <img src="{{ asset('img/view.svg') }}" alt="">
                        </a>

                    </div>
                    <div x-show="rua">
                        <img src="{{ asset('img/visao rua.png') }}"
                            class="object-cover w-full h-96 rounded-md mb-6 z-10" />

                        <a type="button" @click="mapa=true;rua=false" class="flex items-center justify-center gap-1 bg-primary py-3 px-4
                            rounded-pill leading-3 font-semibold text-Xxxs text-neutral-8 absolute top-6 right-8
                            tracking-wider shadow-1 mb-3 cursor-pointer
                            hover:bg-primary-3
                            focus:outline-none
                            disabled:bg-primary-6 disabled:pointer-events-none">
                            Visão do mapa
                            <img src="{{ asset('img/view.svg') }}" alt="">
                        </a>
                    </div>

                    <a type="button" @click="open_loc=true" type="button" class="flex items-center justify-center gap-2 absolute bottom-6 right-8
                            p-3 bg-neutral-1 bg-opacity-80 rounded-full cursor-pointer
                            text-white text-Xxxs font-semibold leading-3
                            ">
                        <img src="{{ asset('img/size.svg') }}" alt="">
                    </a>
                </div>

                <div class="md:hidden relative">
                    <img src="{{ asset('img/view.png') }}" class="object-cover w-full h-60 rounded-md mb-6 z-10" />
                    <a type="button" @click="open_loc=true" type="button" class="flex items-center justify-center gap-2 absolute bottom-2 right-2
                            p-3 bg-neutral-1 bg-opacity-80 rounded-full cursor-pointer
                            text-white text-Xxxs font-semibold leading-3
                            ">
                        <img src="{{ asset('img/size.svg') }}" alt="">
                    </a>
                </div>

                <div class="md:hidden">
                    <p class=" text-Xxs text-neutral-1 font-semibold">Rua Ney da Gama Ahrends</p>
                    <p class="text-Xxs text-neutral-3">Alto Petrópolis, Porto Alegre</p>
                </div>
            </div>
            <hr class=" pb-8 md:pb-0">

            {{-- SIMILARES --}}
            <div class="pb-8 md:py-14">
                <div class="flex justify-between">
                    <label class="text-neutral-1 text-h4 md:text-h3 font-semibold">Imóveis similares</label>
                    <div class="hidden md:flex md:pb-4">
                        <button id="back" class="mr-4 w-xxs h-xxs flex items-center justify-center p-4
                        rounded-full leading-3 font-semibold text-Md text-primary
                        tracking-wider leading-3ring-inset cursor-pointer
                        hover:text-primary-3
                        focus:outline-none focus:bg-neutral-7
                        disabled:bg-primary-6 disabled:text-primary-6
                        disabled:pointer-events-none">
                            < </button>
                                <button id="next" class=" w-xxs h-xxs flex items-center justify-center p-4
                        rounded-full leading-3 font-semibold text-Md text-primary
                        tracking-wider ring-inset cursor-pointer
                        hover:text-primary-3
                        focus:outline-none focus:bg-neutral-7
                        disabled:bg-primary-6 disabled:text-primary-6
                        disabled:pointer-events-none">
                                    >
                                </button>
                    </div>
                </div>


                <div class="flex whitespace-nowrap overflow-auto gap-x-4" id="similares">

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- CARD --}}
                    <div class="flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <hr class=" pb-8 md:pb-0">
        </div>

        {{-- Quero esse anúncio para o meu imóvel! --}}
        <div class="bg-primary text-white md:py-6 md:px-8 py-4 px-6 md:static w-full
                    md:rounded-md md:mb-32 md:grid md:grid-cols-4 md:my-14">
            <div class="col-span-3">
                <p class="text-Xxs font-bold leading-3 mb-2 md:font-SemiBold md:text-h2 ">Quero esse anúncio
                    para o meu imóvel!</p>
                <p class="md:text-Xxs leading-4 mb-6 max-w-xl">Anuncie na Agenciou e receba um anúncio
                    profissional, além de um
                    auxílio
                    na precificação e divulgação do seu imóvel em mais de 100 imobiliárias.</p>

                <button type="button" class="flex items-center justify-center py-3 px-4 w-full
                                rounded-pill leading-3 font-semibold text-Xxxs md:text-Sm text-primary
                                tracking-wider ring-inset bg-white md:w-96
                                hover:text-primary-3 cursor-pointer
                                focus:outline-none focus:bg-neutral-7
                                disabled:bg-primary-6 disabled:text-primary-6 disabled:pointer-events-none">
                    Quero anunciar meu imóvel
                </button>
            </div>
            <img src="{{ asset('img/img1.svg') }}" alt="" class="m-auto hidden md:block">
        </div>
    </div>

    {{-- FOOTER --}}
    <div
        class="md:mb-0 mb-20 md:py-8 md:px-16 p-4 grid grid-cols-1 md:grid-cols-12 items-center text-center bg-neutral-7">

        <div class="flex flex-col items-center md:items-start mb-2 w-full col-span-3">
            <img src="{{ asset('img/logo-neutral03.svg') }}" class=" w-32 h-9">
            <div class="flex justify-evenly md:justify-between items-center w-full md:w-60 my-4">
                <a href="#"><img src="{{ asset('img/facebook.svg') }}" alt=""></a>
                <a href="#"><img src="{{ asset('img/instagram.svg') }}" alt=""></a>
                <a href="#"><img src="{{ asset('img/linkedin.svg') }}" alt=""></a>
                <a href="#"><img src="{{ asset('img/youtube.svg') }}" alt=""></a>
                <a href="#"><img src="{{ asset('img/whatsapp.svg') }}" alt=""></a>
                <a href="#"><img src="{{ asset('img/pinterest.svg') }}" alt=""></a>
            </div>
            <p class="text-neutral-2 md:text-Caption text-Caption2">© 2021 Agenciou! Todos os direitos reservados.</p>
        </div>

        <div class=" text-neutral-2 flex flex-col items-end gap-2 col-span-9 md:text-Caption text-Caption2">
            <p>Tecnopuc – Av. Ipiranga, 6681 – Prédio 96E, Sala 130 – Petrópolis, Porto Alegre/RS - 90619-900 (Creci
                25377-J)</p>
            <p>Rua Rócio, 109 - Mithub, 6° andar, Vila Olímpia, São Paulo/SP - 04552-000 (Creci 201894-F)</p>
            <p>Av. Barão do Rio Branco, 669, São José dos Campos/SP - 12242-800 (Creci 24220-J)</p>
        </div>

    </div>

    {{-- VALOR FIXO MOBILE --}}
    <div
        class="w-full bg-white px-4 pt-3 pb-4 flex justify-between items-center fixed bottom-0 md:hidden border-t-2 border-neutral-6">
        <div>
            <label class="block text-neutral-3 text-sm leading-4">Valor de venda</label>
            <label class="block text-neutral-1 text-base leading-3 font-semibold">R$ 1.280.000,00</label>
        </div>
        <div>
            <a type="button" @click="open_agend=true" class="flex items-center justify-center bg-primary py-3 px-4
                rounded-pill leading-3 font-semibold text-Xxs text-neutral-8
                tracking-wider shadow-1 cursor-pointer
                hover:bg-primary-3
                focus:outline-none
                disabled:bg-primary-6 disabled:pointer-events-none">
                Agendar visita
            </a>
        </div>
    </div>

    {{-- Paginas --}}
    <div>
        {{-- GALERIA FOTOS --}}

        <div x-data="{galeria_cond: false, galeria_imovel: true}" x-show="open_galery"
            class="absolute inset-0 bg-white w-full h-full delay-75 overflow-x-auto">

            <nav class="flex justify-between items-center py-1 px-8 text-neutral-2 font-medium shadow-1">
                <div class="flex items-center">
                    <a @click="open_galery=false" class="cursor-pointer flex items-center">
                        <img src="{{ asset('img/back.svg') }}" alt="" class="p-3">
                        <label class="py-2 cursor-pointer">Voltar</label>
                    </a>
                </div>

                {{-- imóvel / condominio --}}
                <div class="hidden md:flex">
                    <div class="p-2"><a @click="galeria_imovel=true;galeria_cond=false"
                            class="cursor-pointer text-primary font-medium text-h4 leading-4 p-2"
                            :class="galeria_imovel ? 'border-b-2 border-primary' : ' '">
                            Fotos Imóvel</a></div>
                    <div class="p-2"><a @click="galeria_imovel=false;galeria_cond=true"
                            class="cursor-pointer text-primary font-medium text-h4 leading-4 p-2"
                            :class="galeria_cond ? 'border-b-2 border-primary' : ' '">Fotos
                            Condomínio</a></div>
                </div>


                <div class="flex items-center">
                    <a href="" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/favorite.svg') }}">
                    </a>
                    <a href="" @click="open_share=true" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/share.svg') }}">
                    </a>
                </div>
            </nav>

            {{-- imóvel / condominio mobile --}}
            <div class="md:hidden flex items-center justify-center">
                <div class="p-4 pr-2"><a @click="galeria_imovel=true;galeria_cond=false"
                        class="cursor-pointer text-primary font-medium text-h4 leading-4 p-2"
                        :class="galeria_imovel ? 'border-b-2 border-primary' : ' '">
                        Fotos Imóvel</a></div>
                <div class="p-4 pl-2"><a @click="galeria_imovel=false;galeria_cond=true"
                        class="cursor-pointer text-primary font-medium text-h4 leading-4 p-2"
                        :class="galeria_cond ? 'border-b-2 border-primary' : ' '">Fotos
                        Condomínio</a></div>
            </div>

            {{-- FOTOS IMOVEL --}}
            <div x-show="galeria_imovel" class="mt-10 md:mt-0 mb-9 overflow-auto">

                <div class="flex items-center justify-center gap-4 mb-40 md:mb-0">

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides(-1)">

                            <img src="{{ asset('img/prev.svg') }}">

                        </button>
                    </div>

                    <div class="relative my-auto">
                        <button onclick="plusSlides(-1)"
                            class="absolute md:hidden bottom-0 left-0 w-3/6 h-full focus:outline-none"></button>
                        <button onclick="plusSlides(1)"
                            class="absolute md:hidden bottom-0 right-0 w-3/6 h-full focus:outline-none"></button>
                        <button></button>
                        <div class="mySlides fade">
                            <img src="{{ asset('img/1.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 1/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/2.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md  md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 1/8
                            </div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 2/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/3.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-6 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 3/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/4.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 4/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/5.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 5/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/6.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 6/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="{{ asset('img/7.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 7/8
                            </div>
                        </div>

                        <div class="mySlides fade">
                            <img src="img/8.png" class="md:object-cover md:h-96 md:rounded-md md:max-w-2xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 8/8
                            </div>
                        </div>
                    </div>

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides(1)">

                            <img src="{{ asset('img/next.svg') }}" class="">

                        </button>
                    </div>

                </div>

                <div
                    class="flex items-center justify-center gap-2 max-w-6xl whitespace-nowrap overflow-auto p-3 m-auto">
                    <img src="{{ asset('img/1.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(1)">
                    <img src="{{ asset('img/2.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(2)">
                    <img src="{{ asset('img/3.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(3)">
                    <img src="{{ asset('img/4.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(4)">
                    <img src="{{ asset('img/5.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(5)">
                    <img src="{{ asset('img/6.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(6)">
                    <img src="{{ asset('img/7.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(7)">
                    <img src="{{ asset('img/8.png') }}" class="mini w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide(8)">
                </div>
            </div>

            {{-- FOTOS CONDOMINIO --}}
            <div x-show="galeria_cond" class="mt-10 md:mt-0 mb-9">

                <div class="flex items-center justify-center gap-4 mb-40 md:mb-0">

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides_condominio(-1)">

                            <img src="{{ asset('img/prev.svg') }}">

                        </button>
                    </div>

                    <div class="relative my-auto">
                        <button onclick="plusSlides_condominio(-1)"
                            class="absolute md:hidden bottom-0 left-0 w-3/6 h-full focus:outline-none"></button>
                        <button onclick="plusSlides_condominio(1)"
                            class="absolute md:hidden bottom-0 right-0 w-3/6 h-full focus:outline-none"></button>
                        <button></button>
                        <div class="mySlides_condominio fade">
                            <img src="{{ asset('img/condominio/1.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md" style="width:100%">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 1/5
                            </div>
                        </div>

                        <div class="mySlides_condominio fade">
                            <img src="{{ asset('img/condominio/2.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md" style="width:100%">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 2/5
                            </div>
                        </div>

                        <div class="mySlides_condominio fade">
                            <img src="{{ asset('img/condominio/3.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md" style="width:100%">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 3/5
                            </div>
                        </div>

                        <div class="mySlides_condominio fade">
                            <img src="{{ asset('img/condominio/4.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md" style="width:100%">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 4/5
                            </div>
                        </div>

                        <div class="mySlides_condominio fade">
                            <img src="{{ asset('img/condominio/5.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md" style="width:100%">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 5/5
                            </div>
                        </div>

                    </div>

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides_condominio(1)">

                            <img src="{{ asset('img/next.svg') }}" class="">

                        </button>
                    </div>

                </div>

                <div
                    class="flex items-center justify-center gap-2 max-w-6xl whitespace-nowrap overflow-auto p-3 m-auto">
                    <img src="{{ asset('img/condominio/1.png') }}"
                        class="mini_condominio w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide_condominio(1)">
                    <img src="{{ asset('img/condominio/2.png') }}"
                        class="mini_condominio w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide_condominio(2)">
                    <img src="{{ asset('img/condominio/3.png') }}"
                        class="mini_condominio w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide_condominio(3)">
                    <img src="{{ asset('img/condominio/4.png') }}"
                        class="mini_condominio w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide_condominio(4)">
                    <img src="{{ asset('img/condominio/5.png') }}"
                        class="mini_condominio w-40 h-24 rounded-sm cursor-pointer"
                        onclick="currentSlide_condominio(5)">
                </div>
            </div>
        </div>


        {{-- GALERIA FOTOS PLANTA BAIXA --}}

        <div x-show="open_galeria_planta" class="absolute inset-0 bg-white w-full h-full delay-75">

            <nav class="flex justify-between items-center py-1 px-8 text-neutral-2 font-medium shadow-1">
                <div class="flex items-center">
                    <a @click="open_galeria_planta=false" class="cursor-pointer flex items-center">
                        <img src="{{ asset('img/back.svg') }}" alt="" class="p-3">
                        <label class="py-2 cursor-pointer">Voltar</label>
                    </a>
                </div>
                <div class="flex items-center">
                    <a href="" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/favorite.svg') }}">
                    </a>
                    <a href="" @click="open_share=true" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/share.svg') }}">
                    </a>
                </div>
            </nav>

            <div class="mt-10 md:mt-0 mb-9">

                <div class="flex items-center justify-center gap-4 mb-40 md:mb-0">

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides_planta(-1)">

                            <img src="{{ asset('img/prev.svg') }}">

                        </button>
                    </div>

                    <div class="relative my-auto">
                        <button onclick="plusSlides_planta(-1)"
                            class="absolute md:hidden bottom-0 left-0 w-3/6 h-full focus:outline-none"></button>
                        <button onclick="plusSlides_planta(1)"
                            class="absolute md:hidden bottom-0 right-0 w-3/6 h-full focus:outline-none"></button>
                        <button></button>
                        <div class="mySlides_planta fade">
                            <img src="{{ asset('img/planta_baixa/1.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 1/5
                            </div>
                        </div>

                        <div class="mySlides_planta fade">
                            <img src="{{ asset('img/planta_baixa/2.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 3/5
                            </div>
                        </div>

                        <div class="mySlides_planta fade">
                            <img src="{{ asset('img/planta_baixa/3.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 3/5
                            </div>
                        </div>

                        <div class="mySlides_planta fade">
                            <img src="{{ asset('img/planta_baixa/4.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 4/5
                            </div>
                        </div>

                        <div class="mySlides_planta fade">
                            <img src="{{ asset('img/planta_baixa/5.png') }}"
                                class="md:object-cover md:h-96 md:rounded-md md:max-w-xl">
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                ...</div>
                            <div
                                class="hidden md:block max-w-xl text-center m-auto py-3 text-base leading-4 text-neutral-3">
                                Foto 5/5
                            </div>
                        </div>

                    </div>

                    <div class="p-3 bg-primary-6 rounded-full  hover:bg-primary transition delay-75 hidden md:block">
                        <button class="w-7 h-7 flex items-center justify-center focus:outline-none cursor-pointer"
                            onclick="plusSlides_planta(1)">

                            <img src="{{ asset('img/next.svg') }}" class="">

                        </button>
                    </div>

                </div>

                <div
                    class="flex items-center justify-center gap-2 max-w-6xl whitespace-nowrap overflow-auto p-3 m-auto">
                    <img src="{{ asset('img/planta_baixa/1.png') }}"
                        class="mini_planta w-40 h-24 rounded-sm cursor-pointer" onclick="currentSlide_planta(1)">
                    <img src="{{ asset('img/planta_baixa/2.png') }}"
                        class="mini_planta w-40 h-24 rounded-sm cursor-pointer" onclick="currentSlide_planta(2)">
                    <img src="{{ asset('img/planta_baixa/3.png') }}"
                        class="mini_planta w-40 h-24 rounded-sm cursor-pointer" onclick="currentSlide_planta(3)">
                    <img src="{{ asset('img/planta_baixa/4.png') }}"
                        class="mini_planta w-40 h-24 rounded-sm cursor-pointer" onclick="currentSlide_planta(4)">
                    <img src="{{ asset('img/planta_baixa/5.png') }}"
                        class="mini_planta w-40 h-24 rounded-sm cursor-pointer" onclick="currentSlide_planta(5)">
                </div>
            </div>
        </div>


        {{-- AGENDAMENTO --}}
        <div x-show="open_agend" class="absolute inset-0 bg-white w-full delay-75 overflow-auto">
            <nav @click="open_agend=false;open_agen_conf=false"
                class="flex justify-between items-center py-1 px-8 text-neutral-2 font-medium shadow-1">
                <a class="flex items-center cursor-pointer" :class="open_agen_conf ? 'hidden' : '' ">
                    <img src="{{ asset('img/back.svg') }}" alt="" class="p-3">
                    <label class="py-2 cursor-pointer">Voltar</label>
                </a>

                <div :class="open_agen_conf ? '' : 'hidden' "></div>

                <div class="flex items-center justify-end cursor-pointer" :class="open_agen_conf ? '' : 'hidden' ">
                    Compartilhar
                    <a href="" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/share.svg') }}">
                    </a>
                </div>

            </nav>
            {{-- Agendar --}}
            <section x-show="!open_agen_conf"
                class="md:container md:mx-auto md:px-48 md:py-8 md:grid md:grid-cols-3 flex flex-col-reverse px-3">

                <div class="col-span-2 mb-8 md:mb-0">
                    <label class=" font-semibold text-Lg text-neutral-1 hidden md:block mb-8">Agende sua visita</label>
                    <x-form action="#">
                        {{-- DADOS --}}
                        <div>
                            <div class="flex items-center gap-3">
                                <img src="{{ asset('img/user.svg') }}" class="">
                                <label class=" font-semibold text-base text-neutral-1">Informe seus dados</label>
                            </div>

                            <div class="grid gril-cols-1 my-4">
                                <x-label for="Nome_completo" />
                                <x-input name="id_imovel" placeholder="placeholder" />
                            </div>

                            <div class="grid gril-cols-1 my-4">
                                <x-label for="Email" />
                                <x-input name="id_imovel" placeholder="placeholder" />
                            </div>

                            <div class="grid gril-cols-1 my-4">
                                <x-label for="Telefone" />
                                <x-input name="id_imovel" placeholder="placeholder" />
                            </div>
                        </div>
                        {{-- DATA --}}
                        <div class="mt-12">
                            <div class="flex items-center gap-3 mb-4">
                                <img src="{{ asset('img/calendar-primary.svg') }}" class="">
                                <label class=" font-semibold text-base text-neutral-1">Data</label>
                            </div>
                            <label class="text-neutral-3 leading-4 text-base">Selecione o dia para fazer a
                                visita</label>

                            <div class=" max-w-sm my-4">
                                <ul class="grid grid-cols-3 gap-4 text-sm text-neutral-4">
                                    <li class="bg-neutral-6 rounded-xl px-3 py-2 flex items-center justify-center">
                                        <a href="#"> Qua 30/06</a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-3 py-2 flex items-center justify-center">
                                        <a href="#"> Qua 30/06</a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-3 py-2 flex items-center justify-center">
                                        <a href="#"> Qua 30/06</a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-3 py-2 flex items-center justify-center">
                                        <a href="#"> Qua 30/06</a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-3 py-2 flex items-center justify-center">
                                        <a href="#"> Qua 30/06</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {{-- HORA --}}
                        <div class="mt-12  md:max-w-md  xl:max-w-md  2xl:max-w-md" x-data="{fixo: true, flex: false}">
                            <div class="flex items-center gap-3 mb-4">
                                <img src="{{ asset('img/time.svg') }}" class="">
                                <label class=" font-semibold text-base text-neutral-1">Hora</label>
                            </div>
                            <label class="text-neutral-3 leading-4 text-base md:max-w-3xl">
                                Utilize a opção de horário flexível para customizar um horário de visita.
                            </label>

                            <div class="flex mt-2 p-2 text-primary">
                                <a @click="fixo=true;flex=false" class="border-b-2 border-white p-2 cursor-pointer"
                                    :class="{'border-primary' : fixo}">Horário Fixo</a>
                                <div class="p-2">
                                    <a @click="fixo=false;flex=true" class="border-b-2 border-white pb-2 cursor-pointer"
                                        :class="{'border-primary' : flex}">Horário Flexível</a>
                                </div>
                            </div>

                            <div x-show="fixo" class=" max-w-sm my-4">
                                <ul class="grid grid-cols-4 gap-8 text-sm text-neutral-4">
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">8:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">8:30 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">9:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">9:30 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">10:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">10:30 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">8:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">8:30 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">9:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">9:30 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">10:00 </a>
                                    </li>
                                    <li class="bg-neutral-6 rounded-xl px-4 py-2 flex items-center justify-center"><a
                                            href="#">10:30 </a>
                                    </li>
                                </ul>
                            </div>

                            <div x-show="flex" class="flex gap-4 mt-4">
                                <div class="grid grid-cols-1 w-40">
                                    <x-label for="Início da visita" />
                                    <x-input name="id_imovel" placeholder="00:00" />
                                </div>
                                <div class="grid grid-cols-1 w-40">
                                    <x-label for="Fim da visita" />
                                    <x-input name="id_imovel" placeholder="00:00" />
                                </div>
                            </div>
                        </div>
                        {{-- OBS --}}
                        <div class="mt-12 md:max-w-md  xl:max-w-md  2xl:max-w-md">
                            <div class="flex items-center gap-3 mb-4">
                                <img src="{{ asset('img/plan.svg') }}" class="">
                                <label class=" font-semibold text-base text-neutral-1">Detalhes (Opcional)</label>
                            </div>
                            <label class="text-neutral-3 leading-4 text-base">Se houver, insira no campo abaixo alguma
                                observação
                                que
                                seja importante para a realização da sua visita.</label>

                            <div class="my-4">
                                <textarea
                                    class="block border-1.5 border-neutral-4 rounded-md w-80 h-24 m-auto md:m-0 p-3 focus:outline-none"></textarea>
                            </div>
                        </div>
                        {{-- BOTÃO --}}
                        <div class="mx-auto md:mx-0 mt-12 w-52">
                            <a @click="open_agen_conf=true" class=" flex items-center justify-center bg-primary py-3 px-4
                                    rounded-pill leading34 font-semibold md:text-Sm text-Xxxs text-neutral-8
                                    tracking-wider shadow-1 cursor-pointer
                                    hover:bg-primary-3
                                    focus:outline-none
                                    disabled:bg-primary-6 disabled:pointer-events-none">
                                Agendar visita
                            </a>
                        </div>
                    </x-form>

                </div>

                <div>

                    {{-- CARD --}}
                    <div class="hidden md:flex my-4">
                        <div class="w-72 shadow-1 rounded-md">
                            <img src="{{ asset('img/room.png') }}" alt="" class="object-cover h-48 rounded-t-md">
                            <div class=" px-6 py-4">
                                <p class=" font-semibold text-primary text-xl leading-3 mb-4">R$ 1.896.000,00</p>
                                <p class=" font-semibold text-neutral-1 leading-4 mb-1">Rua Visconde do Herval, 204</p>
                                <p class=" text-neutral-3 leading-4 mb-4">Menino Deus, Porto Alegre</p>
                                <div class="flex justify-between">
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/bed.svg') }}" class=" mr-1" alt=""> 2
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/car_neutral.svg') }}" class=" mr-1" alt=""> 1
                                    </label>
                                    <label class="flex text-neutral-4">
                                        <img src="{{ asset('img/area_neutral.svg') }}" class=" mr-1" alt=""> 82m²
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>


                    <label class=" font-semibold text-Lg text-neutral-1 md:hidden block mt-4 mb-8">Agende sua
                        visita</label>
                    {{-- CARD --}}
                    <div class="flex md:hidden rounded-sm shadow-1 my-8">
                        <img src="{{ asset('img/room.png') }}" class="object-cover w-xxl h-24 rounded-l-sm">
                        <div class="py-3 px-4">
                            <p class=" text-neutral-4 leading-1 mb-1">ID 12568</p>
                            <p class=" font-semibold text-neutral-1 leading-3 mb-1">Rua Visconde do Herval, 204</p>
                            <p class=" text-neutral-3 leading-4">Menino Deus, Porto Alegre</p>
                        </div>
                    </div>

                </div>

            </section>

            {{-- Agendamento confirmado --}}
            <div x-show="open_agen_conf" class="md:container md:mx-auto md:px-48 md:py-8">
                <section class=" px-4 py-8 flex flex-col-reverse md:grid md:grid-cols-4">
                    <div class=" md:col-span-2">
                        <label class="text-neutral-1 font-semibold text-h2 md:text-h1 leading-4">Sua visita foi
                            solicitada!</label>

                        {{-- Detalhes da confirmação --}}
                        <div class="flex flex-col gap-6 mt-10 mb-8">
                            <div>
                                <div class="flex gap-2">
                                    <img src="{{ asset('img/confirm-user.svg') }}" alt="">
                                    <label class="text-neutral-1 text-Xxs font-semibold leading-4">Daniel</label>
                                </div>
                                <label class="text-neutral-3 text-Xxs font-normal leading-4 block ml-8">é o corretor que
                                    vai te
                                    acompanhar nessa visita</label>
                            </div>

                            <div class="flex gap-2">
                                <img src="{{ asset('img/confirm-calendar.svg') }}" alt="">
                                <label class="text-neutral-3 text-Xxs font-normal leading-4">Quinta-feira, 8 de julho,
                                    10:30</label>
                            </div>

                            <div class="flex gap-2">
                                <img src="{{ asset('img/confirm-loc.svg') }}" alt="">
                                <label class="text-neutral-3 text-Xxs font-normal leading-4">Rua Ney da Gama Ahrends,
                                    1730 -
                                    Alto
                                    Petrópolis</label>
                            </div>

                            <div class="flex gap-2">
                                <img src="{{ asset('img/confirm-warn.svg') }}" alt="">
                                <label class="text-neutral-3 text-Xxs font-normal leading-4">
                                    Fique atento ao seu e-mail e SMS para receber a confirmação da visita</label>
                            </div>
                        </div>

                        <div class=" border-2 border-warning-2 py-4 px-6 rounded-md">
                            <div class="flex gap-2 mb-2">
                                <img src="{{ asset('img/warn.svg') }}" alt="">
                                <label class="text-neutral-1 font-semibold leading-3">Segurança em primeiro
                                    lugar</label>
                            </div>
                            <label class=" text-neutral-3 leading-4">
                                Pela saúde de todos, utilize máscara durante todos os momentos da visita,
                                higienize suas mãos com álcool em gel e realize a visita com o menor número
                                de pessoas possível. Nossa equipe está tomando todos os protocolos de segurança
                                contra a Covid-19.
                            </label>
                        </div>
                    </div>

                    <div class="md:col-span-2 w-36 md:w-80 pb-6 mx-auto">
                        <img src="{{ asset('img/calendar-conf') }}.svg" alt="">
                    </div>
                </section>

                <div class="md:grid md:grid-cols-4">
                    <div class="px-4 pb-8 md:col-span-3 flex md:gap-10 gap-8">
                        <a href="/anuncio" type="button" class="hidden md:flex items-center justify-center py-3 px-4
                                rounded-pill leading-4 font-semibold text-Sm text-primary
                                tracking-wider border-2 border-primary ring-inset cursor-pointer
                                hover:text-primary-3 hover:border-primary-3
                                focus:outline-none focus:bg-neutral-7
                                disabled:bg-primary-6 disabled:text-primary-6
                                disabled:pointer-events-none">
                            Ver minhas visitas
                        </a>
                        <a href="/anuncio" type="button" class="md:hidden flex items-center justify-center py-3 px-4
                            rounded-pill leading-4 font-semibold text-Xxxs text-primary
                            tracking-wider border-2 border-primary ring-inset cursor-pointer
                            hover:text-primary-3 hover:border-primary-3
                            focus:outline-none focus:bg-neutral-7
                            disabled:bg-primary-6 disabled:text-primary-6
                            disabled:pointer-events-none">
                            Ver visitas
                        </a>
                        <a href="/anuncio" class=" flex items-center justify-center bg-primary py-3 px-4
                        rounded-pill leading34 font-semibold md:text-Sm text-Xxxs text-neutral-8
                        tracking-wider shadow-1 cursor-pointer
                        hover:bg-primary-3
                        focus:outline-none
                        disabled:bg-primary-6 disabled:pointer-events-none">
                            Buscar mais imóveis
                        </a>
                    </div>
                </div>
            </div>
        </div>

        {{-- LOCALIZACAO --}}
        <div x-show="open_loc" x-data="{open_mapa: true, open_rua: false}"
            class="absolute inset-0 bg-white w-full delay-75 overflow-auto">
            <nav class="flex justify-between items-center py-1 px-8 text-neutral-2 font-medium shadow-1">
                <div class="flex items-center">
                    <a @click="open_loc=false" class="cursor-pointer flex items-center">
                        <img src="{{ asset('img/back.svg') }}" alt="" class="p-3">
                        <label class="py-2 cursor-pointer">Voltar</label>
                    </a>
                </div>
                <div class="flex items-center">
                    <a href="" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/favorite.svg') }}">
                    </a>
                    <a @click="open_share=true" class="p-3 cursor-pointer">
                        <img src="{{ asset('img/share.svg') }}">
                    </a>
                </div>
            </nav>

            {{-- VISÃO MAPA --}}
            <div x-show="open_mapa" class="flex items-center relative gap-1">
                <div class="hidden md:block py-4 px-6 absolute top-8 left-8 bg-white rounded-md w-1/4 shadow-1">
                    <div class="font-semibold text-base leading-3">Rua Ney da Gama Ahrends</div>
                    <div class="text-base leading-4">Alto Petrópolis, Porto Alegre</div>
                </div>
                <div>
                    <div class="">
                        <img src="{{ asset('img/visao mapa.png') }}" class=" w-screen object-cover"
                            style="height: 92vh">
                    </div>
                    <a @click="open_mapa=false;open_rua=true"
                        class="flex items-center justify-center gap-2 absolute top-8 right-8 px-4 py-3
                            bg-primary rounded-full text-white text-xl font-semibold leading-3 cursor-pointer shadow-6 hover:bg-primary-3">
                        Visão da rua <img src="{{ asset('img/olho.svg') }}" alt="">
                    </a>
                </div>
            </div>

            {{-- VISÃO RUA --}}
            <div x-show="open_rua" class="flex items-center relative gap-1">
                <div class="hidden md:block py-4 px-6 absolute top-8 left-8 bg-white rounded-md w-1/4 shadow-1">
                    <div class="font-semibold text-base leading-3">Rua Ney da Gama Ahrends</div>
                    <div class="text-base leading-4">Alto Petrópolis, Porto Alegre</div>
                </div>
                <div>
                    <img src="{{ asset('img/visao rua.png') }}" class=" w-screen object-cover" style="height: 92vh">
                    <a @click="open_mapa=true;open_rua=false"
                        class="flex items-center justify-center gap-2 absolute top-8 right-8 px-4 py-3
                            bg-primary rounded-full text-white text-xl font-semibold leading-3 cursor-pointer shadow-6 hover:bg-primary-3">
                        Mapa <img src="{{ asset('img/mapa.svg') }}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
    {{-- <livewire:loader/>
    @livewireScripts --}}
    <script>
        addEventListener('click', (event) => {
            if (event.target.id === 'next') {
                document.getElementById('similares').scrollBy(500, 0);
            };
            if (event.target.id === 'back') {
                document.getElementById('similares').scrollBy(-500, 0);
            };
        })

        var slideIndex = 1;
        showSlides(slideIndex);

        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("mini");
            if (n > slides.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }

        /* ------------- GBR FOTOS CONDOMINIO ----------------- */

        var slideIndex_condominio = 1;
        showSlides_condominio(slideIndex_condominio);

        function plusSlides_condominio(n) {
            showSlides_condominio(slideIndex_condominio += n);
        }

        function currentSlide_condominio(n) {
            showSlides_condominio(slideIndex_condominio = n);
        }

        function showSlides_condominio(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides_condominio");
            var mini_condominio = document.getElementsByClassName("mini_condominio");
            if (n > slides.length) {
                slideIndex_condominio = 1
            }
            if (n < 1) {
                slideIndex_condominio = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < mini_condominio.length; i++) {
                mini_condominio[i].className = mini_condominio[i].className.replace(" active", "");
            }
            slides[slideIndex_condominio - 1].style.display = "block";
            mini_condominio[slideIndex_condominio - 1].className += " active";
        }


        /* -------------- gbr fotos planta baixa ----------------- */

        var slideIndex_planta = 1;
        showSlides_planta(slideIndex_planta);

        function plusSlides_planta(n) {
            showSlides_planta(slideIndex_planta += n);
        }

        function currentSlide_planta(n) {
            showSlides_planta(slideIndex_planta = n);
        }

        function showSlides_planta(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides_planta");
            var mini_planta = document.getElementsByClassName("mini_planta");
            if (n > slides.length) {
                slideIndex_planta = 1
            }
            if (n < 1) {
                slideIndex_planta = slides.length
            }
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < mini_planta.length; i++) {
                mini_planta[i].className = mini_planta[i].className.replace(" active", "");
            }
            slides[slideIndex_planta - 1].style.display = "block";
            mini_planta[slideIndex_planta - 1].className += " active";
        }
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
